package services

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"user_interest/ui_go_api_gateway/config"
	"user_interest/ui_go_api_gateway/genproto/auth_service"
	"user_interest/ui_go_api_gateway/genproto/content_service"
	"user_interest/ui_go_api_gateway/genproto/user_service"
)

type ServiceManagerI interface {
	UserService() user_service.AdminUserServiceClient
	AuthUserService() auth_service.UserServiceClient
	SessionService() auth_service.SessionServiceClient
	FaqService() content_service.FAQServiceClient
}

type grpcClients struct {
	userService     user_service.AdminUserServiceClient
	authUserService auth_service.UserServiceClient
	sessionService  auth_service.SessionServiceClient
	faqService      content_service.FAQServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// User Service...
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Auth Service...
	connAuthService, err := grpc.Dial(
		cfg.AuthServiceHost+cfg.AuthGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Content Service...
	connContentService, err := grpc.Dial(
		cfg.ConentServiceHost+cfg.ConentGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService:     user_service.NewAdminUserServiceClient(connUserService),
		authUserService: auth_service.NewUserServiceClient(connAuthService),
		sessionService:  auth_service.NewSessionServiceClient(connAuthService),
		faqService:      content_service.NewFAQServiceClient(connContentService),
	}, nil
}

func (g *grpcClients) UserService() user_service.AdminUserServiceClient {
	return g.userService
}

func (g *grpcClients) AuthUserService() auth_service.UserServiceClient {
	return g.authUserService
}

func (g *grpcClients) SessionService() auth_service.SessionServiceClient {
	return g.sessionService
}

func (g *grpcClients) FaqService() content_service.FAQServiceClient {
	return g.faqService
}
