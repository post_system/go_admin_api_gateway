package config

import "time"

const (
	ProjectID                  = "e04766bc-3228-4cd9-bd22-09e3fa27a6be"
	ClientPlatformAdminPanelID = "7d4a4c38-dd84-4902-b744-0488b80a4c01"
	ClientTypeAdminID          = "5a3818a9-90f0-44e9-a053-3be0ba1e2c01"
	RoleAdminID                = "a1ca1301-4da9-424d-a9e2-578ae6dcde01"
)

var ExpiretAt = time.Now().Add(time.Hour * 24 * 365).Format(time.RFC3339)
