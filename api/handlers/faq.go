package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"user_interest/ui_go_api_gateway/api/http"
	"user_interest/ui_go_api_gateway/genproto/content_service"
	"user_interest/ui_go_api_gateway/pkg/util"
)

// CreateFaq godoc
// @ID create_faq
// @Router /faq [POST]
// @Summary Create Faq
// @Description  Create Faq
// @Tags Faq
// @Accept json
// @Produce json
// @Param profile body content_service.CreateFAQRequest true "CreateFaqRequestBody"
// @Success 200 {object} http.Response{data=content_service.FAQ} "GetFaqBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateFaq(c *gin.Context) {

	var faq content_service.CreateFAQRequest

	err := c.ShouldBindJSON(&faq)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.FaqService().Create(
		c.Request.Context(),
		&faq,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetFaqByID godoc
// @ID get_faq_by_id
// @Router /faq/{id} [GET]
// @Summary Get Faq  By ID
// @Description Get Faq  By ID
// @Tags Faq
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=content_service.FAQ} "FaqBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetFaqByID(c *gin.Context) {

	faqID := c.Param("id")

	if !util.IsValidUUID(faqID) {
		h.handleResponse(c, http.InvalidArgument, "faq id is an invalid uuid")
		return
	}

	resp, err := h.services.FaqService().GetByID(
		context.Background(),
		&content_service.FAQPrimaryKey{
			Id: faqID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetFaqList godoc
// @ID get_faq_list
// @Router /faq [GET]
// @Summary Get Faq s List
// @Description  Get Faq s List
// @Tags Faq
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=content_service.GetFAQListRequest} "GetAllFaqResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetFaqList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.FaqService().GetList(
		context.Background(),
		&content_service.GetFAQListRequest{
			Limit:  int32(limit),
			Offset: int32(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateFaq godoc
// @ID update_faq
// @Router /faq/{id} [PUT]
// @Summary Update Faq
// @Description Update Faq
// @Tags Faq
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body content_service.UpdateFAQRequest true "UpdateFaqRequestBody"
// @Success 200 {object} http.Response{data=content_service.FAQ} "Faq data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateFaq(c *gin.Context) {

	var faq content_service.UpdateFAQRequest

	faq.Id = c.Param("id")

	if !util.IsValidUUID(faq.Id) {
		h.handleResponse(c, http.InvalidArgument, "faq id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&faq)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.FaqService().Update(
		c.Request.Context(),
		&faq,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteFaq godoc
// @ID delete_faq
// @Router /faq/{id} [DELETE]
// @Summary Delete Faq
// @Description Delete Faq
// @Tags Faq
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Faq data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteFaq(c *gin.Context) {

	faqId := c.Param("id")

	if !util.IsValidUUID(faqId) {
		h.handleResponse(c, http.InvalidArgument, "faq id is an invalid uuid")
		return
	}

	resp, err := h.services.FaqService().Delete(
		c.Request.Context(),
		&content_service.FAQPrimaryKey{Id: faqId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
